﻿using FlightsProject;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RabbitConsumer
{
    class Program
    {

        static void HandleRequest(object model, BasicDeliverEventArgs e)
        {
            // "{ Name: 'itayhau', Password: '1234', Method: 'GetMyTickets' }"
            var body = e.Body.ToArray();
            var message = Encoding.UTF8.GetString(body);
            Console.WriteLine(" [x] Received {0}", message);

            // do work ...
            Task.Run(() =>
            {
                // "{ Name: 'itayhau', Password: '1234', Method: 'GetMyTickets' }"
                // turn json string to object 
                // Name : 'itayhau'
                // Password : '1234'
                // Method: 'GetMyTickets'

                // example:
                FlightsCenterSystem fcs = new FlightsCenterSystem(); // change to Singleton!!!!!!!!
                //if (method == 'GetMyTickets') // check the method name from the message
                CustomerFacade facade = fcs.Login("", "", out LoginToken token); // bring the user name + pwd from the message
                string result = facade.GetAllTickets(token);
                Console.WriteLine(result);
            });
        }

        static void Main(string[] args)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "flight_center_requests",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += HandleRequest;

                channel.BasicConsume(queue: "flight_center_requests",
                                     autoAck: true,
                                     consumer: consumer);

                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }

        }
    }
}
