﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMvvm2403
{
    public class Person : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string m_name;
        public string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                m_name = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }

        // Add Age property + PropertyChanged

        // add function that is doing :
        // NotifyPropertyChanged(string property_name)
        // if (PropertyChanged != null)
           // {
              //PropertyChanged(this, new PropertyChangedEventArgs("Name"));
        //}
        // call this function from Age + Name set property


    public override string ToString()
        {
            return $"Person {Name}";
        }
    }
}
