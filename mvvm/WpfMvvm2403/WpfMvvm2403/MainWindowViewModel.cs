﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfMvvm2403
{
    public class MainWindowViewModel
    {
        public Person MyPerson { get; set; }

        public int Number { get; set; } // remove this

        public MainWindowViewModel()
        {
            MyPerson = new Person { Name = "Danny" };
            Number = 1;
        }

        public void ChangePersonName(string newName)
        {
            MyPerson.Name = newName;
            // also age = age + 1
        }
    }
}
